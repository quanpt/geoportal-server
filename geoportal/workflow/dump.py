#!/usr/bin/python

import sys, os, datetime, decimal

# http://initd.org/psycopg/docs/usage.html
import psycopg2

# http://api.mongodb.org/python/current/tutorial.html
from pymongo import MongoClient

# http://book.py2neo.org/en/latest/fundamentals/#node-relationship-abstracts
from py2neo import neo4j, node, rel

# global connections for different dbs
global pg_conn                # for pg
global res_collection         # for mg
global neo_graph, uuid2record # for neo
global userid2agent, uuid2harvest
uuid2record = {}
uuid2harvest = {}
userid2agent = {}

# reset db command
# neo4j (http://localhost:7474/browser/): match (n) optional match (n)-[r]-() delete n, r
# mongodb (mongo): use cinergi; db.cinergi_resource.remove({})

# --- create connection to different dbs ---
def pg_connect():
  global pg_conn
  pg_conn = psycopg2.connect("dbname=postgres")
  pg_cur = pg_conn.cursor()
  pg_cur.execute("set search_path to 'geoportal'")
  pg_cur.close()

def mg_connect():
  global res_collection
  client = MongoClient('mongodb://localhost:27017/')
  db = client.cinergi
  res_collection = db.cinergi_resource
  
  # reset the mg colelction - TODO: REMOVE THIS
  try:
    db.cinergi_resource.remove({})
  except Exception:
    pass
  
def neo_connect():
  global neo_graph
  neo_graph = neo4j.GraphDatabaseService("http://localhost:7474/db/data/")
  
  # reset the neo graph - TODO: REMOVE THIS
  neo4j.CypherQuery(neo_graph, "MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n, r").execute()

# --- close connection to dbs (if needed) ---
def pg_close():
  pg_conn.close()
  
# --- done ---


# --- neo4j functions ---
def neo_dump_prov(resource):
  global neo_graph, uuid2record
  
  a_node, = neo_graph.create(resource)
  a_node.add_labels("Entity", "Record")
  
  siteuuid = resource['siteuuid']
  if siteuuid == '':
    # don't have parent, link to user
    u_dn, u_username = pg_get_user(resource['owner'])
    query = neo4j.CypherQuery(neo_graph, "MATCH (ee:Agent) WHERE ee.username = {p_username} RETURN ee;")
    agent = query.execute_one(p_username = u_username)
    if agent is None:
      agent, _ = neo_graph.create( \
          node(userid = resource['owner'], dn = u_dn, username = u_username), \
          rel(a_node, "wasAttributedTo", 0))
      agent.add_labels("Agent")
    else:
      neo_graph.create(rel(a_node, "wasAttributedTo", agent))
  else:
    # has parent
    # link to its resourceURI node
    try:
      parent_node = uuid2record[siteuuid]
    except KeyError:
      query = neo4j.CypherQuery(neo_graph, "MATCH (ee:Entity) WHERE ee.docuuid = {p_uuid} RETURN ee;")
      parent_node = query.execute_one(p_uuid = siteuuid)
      if not parent_node is None:
        uuid2record[siteuuid] = parent_node
        parent_node.add_labels("Resource")
        parent_node.remove_labels("Record")
    if not parent_node is None:
      neo_graph.create(rel(a_node, "wasAssociatedWith", parent_node))
    
    # and link to harvest node
    try:
      harvest = uuid2harvest[resource['juuid']]
      neo_graph.create(rel(a_node, "wasGeneratedBy", harvest))
    except KeyError:
      print "Harvest node not found!"

def neo_update_user():
  global neo_graph, userid2agent
  print "neo_update_user ..."
  all_users = pg_get_all_users()
  for (u_id, u_dn, u_username) in all_users:
    query = neo4j.CypherQuery(neo_graph, "MATCH (ee:Agent) WHERE ee.username = {p_username} RETURN ee;")
    agent = query.execute_one(p_username = u_username)
    if agent is None:
      agent, = neo_graph.create( \
          node(userid = u_id, dn = u_dn, username = u_username))
      agent.add_labels("Agent")
      userid2agent[u_id] = agent
  
def neo_update_harvest():
  global neo_graph, uuid2harvest
  print "neo_update_harvest ..."
  all_harvest = pg_get_all_harvest()
  field_names = "uuid, harvest_id, input_date, harvest_date, job_type, service_id, agent".split(", ")
  for record in all_harvest:
    query = neo4j.CypherQuery(neo_graph, "MATCH (ee:Harvest) WHERE ee.uuid = {p_uuid} RETURN ee;")
    harvest = query.execute_one(p_uuid = record[0])
    if harvest is None:
      harvest_dict = {}
      for i in range(len(field_names)):
        harvest_dict[field_names[i]] = record[i]
      
      harvest, = neo_graph.create(harvest_dict)
      harvest.add_labels("Harvest", "Activity")
      uuid2harvest[record[0]] = harvest
      try:
        agent = userid2agent[harvest_dict['agent']]
        neo_graph.create(rel(harvest, "wasAssociatedWith", agent))
      except KeyError:
        pass

def neo_update_harvest_resource():
  global neo_graph, uuid2harvest
  print "neo_update_harvest_resource ..."
  for harvest in uuid2harvest.values():
    query = neo4j.CypherQuery(neo_graph, "MATCH (ee:Resource) WHERE ee.docuuid = {p_uuid} RETURN ee;")
    resource = query.execute_one(p_uuid = harvest['harvest_id'])
    if resource is None:
      print harvest['harvest_id'], "not found"
    else:
      neo_graph.create(rel(harvest, "used", resource))
      
  
# --- pg functions ---

def pg_get_all(sql):
  global pg_conn
  pg_cur = pg_conn.cursor()
  pg_cur.execute(sql)
  record = pg_cur.fetchall()
  pg_cur.close()
  return record
  
# input: userid
# return dn, username
def pg_get_user(userid):
  global pg_conn
  pg_cur = pg_conn.cursor()
  pg_cur.execute("""SELECT dn, username FROM gpt_user
      WHERE userid = %s;""", (userid,))
  record = pg_cur.fetchone()
  pg_cur.close()
  return record[0], record[1]
  
# return list of userid, dn, username from pg
def pg_get_all_users():
  return pg_get_all("SELECT userid, dn, username FROM gpt_user")

# return list of uuid, harvest_id, input_date, harvest_date, job_type, service_id, agent from pg
def pg_get_all_harvest():
  return pg_get_all("""SELECT uuid, harvest_id, input_date, harvest_date, 
  job_type, service_id, agent FROM gpt_harvesting_jobs_completed""")


def file_dump_record(filename, xml):
  with open(filename, "w") as xml_file:
    xml_file.write(xml)

def pg_dump_uuid(uuid, dump_dir = "./"):
  global pg_conn
  pg_cur = pg_conn.cursor()
  
  # get and dump all xml files
  pg_cur.execute("""SELECT r.*, rd.xml FROM gpt_resource r
      INNER JOIN gpt_resource_data rd ON r.docuuid = rd.docuuid 
      WHERE r.docuuid = %s;""", (uuid,))
  record = pg_cur.fetchone()
  
  if record is None:
    print "None record: ", uuid
  else:
    field_names, fields = pg_create_fields(pg_cur)
    pg_cur.close()
    
    pg_dump_record(record, fields, field_names, dump_dir)
    
    pg_dump_uuid_children(uuid)

def pg_dump_record(record, fields, field_names, dump_dir):
  global res_collection
  
  #~ print record[fields['docuuid']][2:-2] + ".xml", ":", len(record[fields['xml']])
    
  # write to xml files
  file_dump_record(dump_dir + record[0][2:-2] + ".xml", \
      record[fields['xml']])
  
  # make a dict of this record for mongodb and neo4j
  resource = {}
  for i in range(len(field_names)):
    if type(record[i]) == decimal.Decimal:
      resource[field_names[i]] = float(record[i])
    #~ elif type(record[i]) == datetime.datetime:
      #~ resource[field_names[i]] = tuple(record[i].timetuple())[:6]
    else:
      resource[field_names[i]] = record[i]
      
  resource['mongodb_inserttime'] = datetime.datetime.now()
  
  # insert to mongodb
  res_collection.insert(resource)
  
  # insert to neo4j
  del resource['xml'] # I don't need content
  resource['mongodb_id'] = str(resource['_id'])
  del resource['_id']
  neo_dump_prov(resource)


def pg_create_fields(pg_cur):
  field_names = [x.name for x in pg_cur.description]
  fields = {}
  for i in range(len(field_names)):
    fields[field_names[i]] = i
  return (field_names, fields)

# input: uuid in format: {EA0EBD6-2DF5-43D1-8942-38459C2BA90}
# output: record from postgresql (pg) is 
#     + written to xml files
#     + inserted into mongodb (mg)
#     + insert prov into neo4j (neo)
def pg_dump_uuid_children(uuid):
  global pg_conn
  pg_cur = pg_conn.cursor()
  
  # get and dump all xml files
  pg_cur.execute("""SELECT r.*, rd.xml FROM gpt_resource r
      INNER JOIN gpt_resource_data rd ON r.docuuid = rd.docuuid 
      WHERE siteuuid = %s;""", (uuid,))
  all_records = pg_cur.fetchall()
  
  if len(all_records) > 0:
    # make dump directory
    dump_dir = uuid[2:-2] + "/"
    try:
      os.mkdir(dump_dir)
    except OSError:
      pass
  else:
    # no child to dump
    pass
  
  # prepare field index
  field_names, fields = pg_create_fields(pg_cur)
    
  # dump xml files & insert to mongodb
  for record in all_records:
    pg_dump_record(record, fields, field_names, dump_dir)
      
  pg_cur.close()

def main():
  global pg_conn
  pg_connect()
  mg_connect()
  neo_connect()
  
  neo_update_user()
  neo_update_harvest()
  
  # get UUID from input argv, or 1st site UUID in db
  uuid = sys.argv[1] if len(sys.argv) == 2 else ''
  if uuid == '':
    pg_cur = pg_conn.cursor()
    pg_cur.execute("SELECT DISTINCT siteuuid FROM gpt_resource WHERE siteuuid <> ''");
    uuids = pg_cur.fetchall()
    if len(uuids) > 0:
      uuid_list = [u[0] for u in uuids]
    pg_cur.close()
  else:
    if len(uuid) < 38:
      uuid_list = ["{" + uuid + "}"]
    
  if len(uuid_list) == 0:
    print "Cannot find any siteUUID"
    sys.exit(-1)
  
  for uuid in uuid_list:
    pg_dump_uuid(uuid)
  
  neo_update_harvest_resource()
  
  pg_close()
  
if __name__ == '__main__':
  main()
