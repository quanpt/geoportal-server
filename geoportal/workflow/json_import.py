#!/usr/bin/python

import sys, json
from geoprovdm import *

# convert a prov-dm json to a python dictionary object
#   with key and value of type string
def json2obj(ajson):
    res = {}
    for attr in ajson:
        if type(ajson[attr]) is dict:
            if ajson[attr]['type'] == 'xsd:string':
                res[attr] = ajson[attr]['$']
            else:
                res[attr] = str(ajson[attr]['$'])
        else:
            res[attr] = str(ajson[attr])
    return res

def main():
    
    # input file
    filename = 'doc_ingestion_id_assignment_combined_prov.json'
    if len(sys.argv) == 2:
        filename = sys.argv[1]
        
    # read input data
    with open(filename) as content_file:
        obj = json.loads(content_file.read())
    
    # =======================
    # === add all objects ===

    p = GeoProvDM("http://localhost:7474/db/data/", True)
    
    # make all entities
    entities = obj['entity']
    for k in entities.keys():
        entity = json2obj(entities[k])
        entity[u'_id'] = k
        p.addEntity(entity)
    
    # make all agents
    agents = obj['agent']
    for k in agents.keys():
        agent = json2obj(agents[k])
        agent[u'_id'] = k
        p.addAgent(agent)
        
    # make all activities
    acts = obj['activity']
    for k in acts.keys():
        act = json2obj(acts[k])
        act[u'_id'] = k
        p.addActivity(act)

    # =========================
    # === add all relations ===
    for rel in p.getRequiredIdsInRelation().keys():
        try:
            relations = obj[rel]
            for name in relations.keys():
                p.addRelation(rel, name, relations[name])
        except KeyError:
            pass
    
    
if __name__ == '__main__':
    main()
